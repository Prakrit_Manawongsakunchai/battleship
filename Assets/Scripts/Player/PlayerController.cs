﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    private float shipSpeed = 10;
    private Vector2 shipMovementInput = Vector2.zero;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        var newXPos = transform.position.x + (shipMovementInput.x * shipSpeed * Time.deltaTime);
        var newYPos = transform.position.y + (shipMovementInput.y * shipSpeed * Time.deltaTime);
        transform.position = new Vector3(newXPos, newYPos, transform.position.z);
    }
    public void onMove(InputAction.CallbackContext context)
    {
        shipMovementInput = context.ReadValue<Vector2>();
    }
}
